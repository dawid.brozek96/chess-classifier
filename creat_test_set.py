from PIL import Image

def creat_test_easy(figure: str):
    rot_image = Image.open('data_for_test/{}.png'.format(figure)).convert("RGBA")
    for i in range (5):
        rotated = rot_image.rotate(i*90)
        rotated.save('test_set/{}{}.png'.format(figure,  i))


def creat_test(figure: str):
    rot_image = Image.open('data_for_test/{}.png'.format(figure)).convert("RGBA")
    for i in range (65):
        rotated = rot_image.rotate(i*5)
        new_img = Image.new('RGBA', rotated.size, 'white')
        Alpha_Image = Image.composite(rotated, new_img, rotated)
        Alpha_Image = Alpha_Image.convert(rotated.mode)
        Alpha_Image.save('test_set/{}{}.png'.format(figure,  i))


#hard test
creat_test('king')
creat_test('bishop')
creat_test('tower')


#easy test
# creat_test_easy('king')
# creat_test_easy('bishop')
# creat_test_easy('tower')