from PIL import Image


def creat_img(figure: str):
    chees_name = Image.open('data_start/{}.png'.format(figure))
    for k in range(9):
        for i in range(0, 8):
            for j in range (0,8):
                im_board = Image.open('data_start/board.png').convert("RGBA")
                im_board.paste(chees_name, ((100*i)+5, (100*j)+5))

                rotated = im_board.rotate(k * 45)
                new_img = Image.new('RGBA', rotated.size, 'white')
                Alpha_Image = Image.composite(rotated, new_img, rotated)
                Alpha_Image = Alpha_Image.convert(rotated.mode)


                Alpha_Image.save('data/chees/{}/{}{}{}{}.png'.format(figure, figure,k ,i,j))


creat_img('king')
creat_img('bishop')
creat_img('tower')







