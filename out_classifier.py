import tensorflow as tf
from speculation import ratio


def out_tf(figure_name: str):
    model = tf.keras.models.load_model('saved_model/my_model')
    for i in range (4):
        figure = ('test_set/{}{}.png'.format(figure_name,i))
        ratio(figure, model)


def out_tf_f(figure_name: str):
    model = tf.keras.models.load_model('saved_model/my_model')
    for i in range (65):
        figure = ('test_set/{}{}.png'.format(figure_name,i))
        ratio(figure, model)

#easy version
# print('here must be a king')
# out_tf('king')
#
# print('\nhere must be a tower')
# out_tf('tower')
#
#
# print('\nhere must be a bishop')
# out_tf('bishop')


#hard version - make first data test -> go to creat_test_set - > hard test ( creat_test function)
print('here must be a king')
out_tf_f('king')

print('\nhere must be a tower')
out_tf_f('tower')

print('\nhere must be a bishop')
out_tf_f('bishop')



