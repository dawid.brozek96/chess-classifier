from tensorflow import keras
import tensorflow as tf
import numpy as np



class_name = ['bishop', 'king', 'tower']
img_height = 300
img_width = 300

def ratio(path , model):
    img = keras.preprocessing.image.load_img(
        path, target_size=(img_height, img_width)
    )
    img_array = keras.preprocessing.image.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0) # Create a batch

    predictions = model.predict(img_array)
    score = tf.nn.softmax(predictions[0])

    print(
        "This image most likely belongs to {} with a {:.2f} percent confidence."
        .format(class_name[np.argmax(score)], 100 * np.max(score))
    )

